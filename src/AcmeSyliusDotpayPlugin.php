<?php

declare(strict_types=1);

namespace Acme\SyliusDotpayPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class AcmeSyliusDotpayPlugin extends Bundle
{
    use SyliusPluginTrait;
}
