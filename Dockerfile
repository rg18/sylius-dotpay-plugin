FROM php:7.4-fpm
RUN apt-get update && apt-get install -y libpq-dev git yarn zlib1g-dev libzip-dev libpng-dev libjpeg-dev libfreetype6-dev libpq-dev git yarn libicu-dev
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-configure intl
RUN docker-php-ext-install mysqli pdo pdo_mysql zip gd exif intl bcmath
RUN docker-php-ext-enable pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR "/var/www/syliusdotpayplugin"
CMD ["php-fpm"]
